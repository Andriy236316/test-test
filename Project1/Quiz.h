#pragma once
#include<iostream>
#include<string>
using namespace std;

struct Question {
	string questionText;
	string trueAnswer;
};

struct Quiz {
	string Title;
	Question questions[5];
};

void insertQuiz(Quiz quiz);
void init();
void addQuiz();
void showAllQuizes();
void passQuiz();
