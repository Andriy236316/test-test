#include<iostream>
#include<string>
#include "Quiz.h"
using namespace std;

int main() {
	init();
	int action = 0;
	do
	{
		cout << "1. Add new quiz" << endl;
		cout << "2. show and pass quizes" << endl;
		cout << "3. Exit " << endl;
		cout << endl;
		cout << "Select action ->_";
		cin >> action;
		switch (action)
		{
			case 1: {
				addQuiz();
			}break;

			case 2: {
				showAllQuizes();
				passQuiz();
			}break;

			case 3: {
				cout << "Goodbye" << endl;
			}break;

			default: {
				cout << "Error: Please select action from list!" << endl;
			}break;
		}
	} while (action != 3);


	return 0;
}