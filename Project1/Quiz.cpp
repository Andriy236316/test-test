#include "Quiz.h"
#include<iostream>
#include<string>
#include<fstream>
using namespace std;

Quiz* quizes;//��� ��� 3 ��������
int countQuizes = 0; //3 
const int countQestionInQuiz = 5;
const string fileName = "Quizes.txt";

void insertQuiz(Quiz getQuiz)
{
	Quiz* temp = new Quiz[countQuizes + 1];
	for (int i = 0; i < countQuizes; i++)
	{
		temp[i] = quizes[i];
	}
	
	temp[countQuizes] = getQuiz; //�������� ������� � �����

	countQuizes++; 
	quizes = new Quiz[countQuizes];
	for (int i = 0; i < countQuizes; i++)
	{
		quizes[i] = temp[i];
	}
	delete[]temp;
}



void init()
{
	ifstream fin;
	fin.open(fileName);
	bool isOpen = fin.is_open();
	if (isOpen == true) {
		while (!fin.eof())
		{
			Quiz getQuiz;
			fin >> getQuiz.Title;
			for (int i = 0; i < countQestionInQuiz; i++)
			{
				fin >> getQuiz.questions[i].questionText;
				fin >> getQuiz.questions[i].trueAnswer;
			}
			insertQuiz(getQuiz);
		}
	}
	else {
		cout << "Erro: applciation can't open file!" << endl;
	}
}

void addQuiz()
{
	ofstream fout;
	fout.open(fileName, fstream::app);
	bool isOpen = fout.is_open();

	if (isOpen == true) {
		Quiz* temp = new Quiz[countQuizes + 1];
		for (int i = 0; i < countQuizes; i++)
		{
			temp[i] = quizes[i];
		}

		cout << "Enter title for quiz ->_";
		cin >> temp[countQuizes].Title;

		for (int i = 0; i < countQestionInQuiz; i++)
		{
			cout << "Enter " << i + 1 << "/" << countQestionInQuiz << " question ->";
			cin >> temp[countQuizes].questions[i].questionText;

			cout << "Enter correct answer fot this question ->_";
			cin >> temp[countQuizes].questions[i].trueAnswer;
		}

		fout << temp[countQuizes].Title << endl;
		for (int i = 0; i < countQestionInQuiz; i++)
		{
			fout << temp[countQuizes].questions[i].questionText << endl;
			fout << temp[countQuizes].questions[i].trueAnswer << endl;
		}

		countQuizes++; //4
		quizes = new Quiz[countQuizes];
		for (int i = 0; i < countQuizes; i++)
		{
			quizes[i] = temp[i];
		}
		delete[]temp;
	}
	else {
		cout << "Error: Application can't open file!" << endl;
	}
}

void showAllQuizes()
{
	if (countQuizes != 0) {
		for (int i = 0; i < countQuizes; i++)
		{
			cout << i + 1 << ". " << quizes[i].Title << endl;
		}
	}
	else {
		cout << "List quizes is empty!" << endl;
	}
	
}

void passQuiz()
{
	int selectQuiz = 0;
	int score = 0;

	cout << "Enter number quiz for start passing ->_";
	cin >> selectQuiz;
	selectQuiz--;

	cout << endl;
	cout << "You started passing " << quizes[selectQuiz].Title << endl;
	for (int i = 0; i < countQestionInQuiz; i++)
	{
		string tempAnswer = "";
		cout << i + 1 << ". " << quizes[selectQuiz].questions[i].questionText << endl;
		cout << "Your answer ->_";
		cin >> tempAnswer;

		if (tempAnswer == quizes[selectQuiz].questions[i].trueAnswer){
			score += 20;
			cout << "It's true answer (+20 score)" << endl;
		}
		else {
			cout << "It's false answer" << endl;
		}
	}
	cout << "Your result: " << score << "/100" << endl;
}
